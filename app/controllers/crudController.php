<?php
class Crud extends Controller {
    
    public function add(){
        $db = new crudModel();
        
        if(isset($_POST['enviar'])){
            $db->save(array(
                'title' => $_POST['title'],
                'description' => $_POST['description'],
                'body' => $_POST['body'],
                'author' => $_POST['author'],
            ));
            $this->redirect();
        }
        
        $this->view('crudAdd');
    }
    
    public function delete(){
        $db = new crudModel();
        if($db->delete('id='.$this->getParam('id'))){
            $this->redirect();
        }
    }
    
    public function read(){
        $db = new crudModel();
        $d['dados'] = $db->find('id='.$this->getParam('id'));
        $this->view('crudView', $d);
    }
    
    public function edit(){
        $db = new crudModel();
        $id = $this->getParam('id');
        if(isset($_POST['enviar'])){
            $db->update(array(
                'title' => $_POST['title'],
                'description' => $_POST['description'],
                'body' => $_POST['body'],
                'author' => $_POST['author'],
            ), 'id='.$_POST['id']);
            $this->redirect();
        } else {
            $d['dados'] = $db->find('id='.$id);
            $this->view('crudEdit', $d);
        }
    }
}
