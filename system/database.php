<?php
class Database {
    protected $conn;
    
    public $db = array(
        'host' => '127.0.0.1',
        'login' => 'root',
        'password' => '',
        'database' => 'ignicaodigital',
        'charset' => 'utf8'
    );
    
    public function __construct(){
        $this->conn = new PDO('mysql:host='.$this->db['host'].';dbname='.$this->db['database'], $this->db['login'], $this->db['password'], 
                array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES ' . $this->db['charset']
            )
        );
    }
}
